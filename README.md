
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Opened Issues](https://img.shields.io/bitbucket/issues-raw/antoineguerra/migrate_my_repo)](https://bitbucket.org/antoineguerra/migrate_my_repo/issues?status=new&status=open)
[![Closed Issues](https://img.shields.io/github/issues-closed/antoineguerra/migrate_my_repo)](https://bitbucket.org/antoineguerra/migrate_my_repo/issues?status=new&status=close)


# Migrate to Bitbucket

This script migrate repositories in bitbucket repositories

## Getting Started

Please run 

    git clone https://antoineguerra@bitbucket.org/antoineguerra/migrate_my_repo.git
    cd migrate_my_repo
    
### Prerequisites

Please be sure `php-pcntl` is installed. Please run :

    php -i | grep pcntl
    # OUTPUT pcntl support => enabled
    
If you have **trouble** with **php pcntl** Please **disable** it in [start.php](start.php) with **PHP_PCNTL constant** 
    
    // start.php
    /**
     * ENABLE|DISABLE PHP CNTL extension
     */
    const PHP_CNTL = false;

Please be sure you've an **ssh** access of all **repositories accounts** you use

### Installing

Please Run :

    php start.php -u YOUR_BITBUCKET_USER --git-url='GIT_URL_WHERE_CLONE' 
    
          

Available Options :

- `--simple-repo=YOUR_REPOSITORY` Repository name 
- `--file=./YOUR_FILE.YOUR_EXT_LIKE_json` File where find repositories like **.json** OR **use pattern**
- `--pattern=/YOUR_PATTERN\:.*/` **Regex Pattern** to **find repositories** in your **file**
- `--pattern-modifier=gi` **Regex pattern modifier** [Please see it here](http://help.stonesoft.com/onlinehelp/StoneGate/SMC/5.3.6/SGAG/SG_RegularExpressions/Pattern-Matching_Modifiers.htm)
- `--push-all-branches` Push all your repositories **branches**
- `--push-all-tags` Push all your repositories **tags** 
- `--private` Create your new repositories in **private** mode
- `--with-wiki` Create your new repositories with a **wiki** 
- `--with-issues` Create your new repositories with an **issues reporting**
- `--owner=YOUR_TEAM` Change the owner of new repositories. Probably if you want use **team**
- `--create-staging` Create new branch named **staging** child of **master** branch

Example with [json file](sample.json) :

    php -r "echo json_encode(['repository_1', 'repository_2'])" > sample.json
    php start.php -u YOUR_BITBUCKET_USER --git-url='git@github.com:AntoineGuerra' --push-all-branches --push-all-tags --private --with-wiki --with-issues --create-staging --file=sample.json
    
Example Simple repo :
```
php start.php -u YOUR_BITBUCKET_USER --git-url='git@github.com:AntoineGuerra' --simple-repo=start
```

Example with [sample file](sample) :
```
php start.php -u YOUR_BITBUCKET_USER --git-url='git@github.com:AntoineGuerra' --push-all-branches --push-all-tags --private --with-wiki --with-issues --create-staging --file=./sample --pattern=/repository\\s*\\:\\s*([^\\s]*)\\s*/ --pattern-modifier=i
```

### Logs and repositories

Log save in [tmp/migrate_my_repo.log](tmp/migrate_my_repo.log)
Repositories save in [tmp/repos](tmp/repos)

## Built With

* [Bitbucket](https://bitbucket.org) - Used to create your repositories
* [JetBrains](https://www.jetbrains.com) - IDE
* [Regex101](https://regex101.com/) - The website to test regex

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Antoine Guerra** - *Initial work* - [BitbucketMigrate](https://github.com/bitbucket_migrate)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


