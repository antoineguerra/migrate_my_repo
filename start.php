#!/usr/bin/php
<?php
/**
 * Created by Agence Mayflower.
 * User: antoine
 * Date: 2019-06-19
 * Time: 14:47
 */
ini_set('display_errors', 'on');
/**
 * Where log will be save
 */
$LOG_FILE = './tmp/' . preg_replace('/\..*$/', '', basename(__DIR__)) . '.log';

/**
 * Where repositories will be clone
 */
const REPO_PATH = './tmp/repos/';

/**
 * ENABLE|DISABLE PHP CNTL extension
 */
const PHP_CNTL = true;
$CONFIG = [];
/**
 *
 */
const STEP = [
    'clone'          => 1,
    'create_repo'    => 2,
    'push_branches'  => 3,
    'push_tags'      => 4,
    'create_staging' => 5,
    'push_staging'   => 6,
    'add_remote'     => 7,
];

$current_repo_url = false;

/**
 * COLORS
 */
class Colors {
    private $foreground_colors = array();
    private $background_colors = array();

    public function __construct() {
        // Set up shell colors
        $this->foreground_colors['black'] = '0;30';
        $this->foreground_colors['dark_gray'] = '1;30';
        $this->foreground_colors['blue'] = '0;34';
        $this->foreground_colors['light_blue'] = '1;34';
        $this->foreground_colors['green'] = '0;32';
        $this->foreground_colors['light_green'] = '1;32';
        $this->foreground_colors['cyan'] = '0;36';
        $this->foreground_colors['light_cyan'] = '1;36';
        $this->foreground_colors['red'] = '0;31';
        $this->foreground_colors['light_red'] = '1;31';
        $this->foreground_colors['purple'] = '0;35';
        $this->foreground_colors['light_purple'] = '1;35';
        $this->foreground_colors['brown'] = '0;33';
        $this->foreground_colors['yellow'] = '1;33';
        $this->foreground_colors['light_gray'] = '0;37';
        $this->foreground_colors['white'] = '1;37';

        $this->background_colors['black'] = '40';
        $this->background_colors['red'] = '41';
        $this->background_colors['green'] = '42';
        $this->background_colors['yellow'] = '43';
        $this->background_colors['blue'] = '44';
        $this->background_colors['magenta'] = '45';
        $this->background_colors['cyan'] = '46';
        $this->background_colors['light_gray'] = '47';
    }

    // Returns colored string
    public function getColoredString($string, $foreground_color = null, $background_color = null) {
        $colored_string = "";

        // Check if given foreground color found
        if (isset($this->foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset($this->background_colors[$background_color])) {
            $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .=  $string . "\033[0m";

        return $colored_string;
    }

    // Returns all foreground color names
    public function getForegroundColors() {
        return array_keys($this->foreground_colors);
    }

    // Returns all background color names
    public function getBackgroundColors() {
        return array_keys($this->background_colors);
    }
}
$colors = new Colors();
/**
 * END COLORS
 */


/**
 * Gracefully Stopping
 */
declare(ticks = 1);

if (PHP_CNTL) {
    pcntl_signal(SIGTERM, 'signalHandler');// Termination ('kill' was called)
    pcntl_signal(SIGHUP, 'signalHandler'); // Terminal log-out
    pcntl_signal(SIGINT, 'signalHandler'); // Interrupted (Ctrl-C is pressed)
}

$pidFileName = 'tmp/.' . basename(__DIR__) . '.pid';
$pidFile = @fopen($pidFileName, 'c');
if (!$pidFile) die("Could not open $pidFileName\n");
if (!@flock($pidFile, LOCK_EX | LOCK_NB)) die("Already running?\n");
ftruncate($pidFile, 0);
fwrite($pidFile, getmypid());

$current_step = 0;
$current_repo = '';
/**
 * ${STATIC} Function : signalHandler
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $signal
 *
 * @return void
 */
function signalHandler($signal) {
    global $pidFile, $current_repo, $current_step, $CONFIG, $colors;

    pcntl_signal(SIGINT, 'kill'); // Interrupted (Ctrl-C is pressed)

    $gracefullySTR = $colors->getColoredString('Gracefully stopping... (press Ctrl+C again to force)', 'green') . PHP_EOL;
    $gracefullySTR .= $colors->getColoredString("Please, keep me finish my job on " . $current_repo . " repository !", 'yellow') . PHP_EOL;
    echo $gracefullySTR;

    switch ($current_step) {
        case STEP['clone']:
            warning_log('Gracefully stopping in Cloning Step remove ' . REPO_PATH . $current_repo . ' directory');
            $start = microtime(true);

            if (is_dir(REPO_PATH . $current_repo)) {
                shell_exec('rm -r ' . REPO_PATH . $current_repo);
            }
            $elapsedTime = microtime(true) - $start;
            save_log('Directory ' . REPO_PATH . $current_repo . ' removed in ' . $elapsedTime . 's');
            ftruncate($pidFile, 0);
            exit;
        case STEP['add_remote']:
            save_log('Gracefully stopping in add remote Step waiting finish step');
            $start = microtime(true);
            if (key_exists('owner', $CONFIG)) {
                add_remote($current_repo, $CONFIG['user'], $CONFIG['owner'], $CONFIG['push-all-branches'], $CONFIG['push-all-tags'], $CONFIG['create-staging']);
            } else {
                add_remote($current_repo, $CONFIG['user'], false, $CONFIG['push-all-branches'], $CONFIG['push-all-tags'], $CONFIG['create-staging']);
            }
            $elapsedTime = microtime(true) - $start;
            ftruncate($pidFile, 0);
            save_log('Gracefully stopping step ended in ' . $elapsedTime . 's, kill process with ' . $pidFile . ' PID');
            exit;
        case STEP['create_repo']:
            echo $colors->getColoredString('Check if repository ' . $current_repo . ' was created in your BitBucket account !', 'red') . PHP_EOL;
            warning_log('Check if repository ' . $current_repo . ' was created in your BitBucket account !');
            ftruncate($pidFile, 0);
            exit;
    }
}

/**
 * ${STATIC} Function : kill
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 *
 * @return void
 */
function kill() {
    global $pidFile, $current_repo, $current_step, $CONFIG, $colors, $current_repo_url;
    $killMessage = $colors->getColoredString('Force KILL Please check your bitbucket account', 'red') . PHP_EOL;
    die($killMessage);
    switch ($current_step) {
        case STEP['add_remote']:
            my_error_log('Force kill in: add remote step');
            echo $colors->getColoredString('Please remove ' . $current_repo . ' in bitbucket' . ($current_repo_url !== false) ? ' => ' . $current_repo_url : '', 'red') . PHP_EOL;
            my_error_log('Please remove ' . $current_repo . ' in bitbucket' . ($current_repo_url !== false) ? ' => ' . $current_repo_url : '');
            ftruncate($pidFile, 0);
            exit;
        case STEP['create_repo']:
            echo $colors->getColoredString('Check if repository ' . $current_repo . ' was created in your BitBucket account !', 'red') . PHP_EOL;
            my_error_log('Check if repository ' . $current_repo . ' was created in your BitBucket account !');
            ftruncate($pidFile, 0);
            exit;
        default:
            exit;
    }
}

/**
 * END GRACEFULLY STOPPING
 */



function help($opt) {
    if (key_exists('h', $opt) || key_exists('help', $opt)) {
        save_log('Help used');
        die("You must use these required options :\n-u BitBucket_USER\n-p BitBucket_PASSWORD\n--git-url=\"GIT_CLONE_URL_WITHOUT_REPO\" like : git@github.com:AntoineGuerra/\n\n" .
            "You can use these optionals options :\n--push-all-branches\n--push-all-tags\n--with-wiki\n--private\n--create-staging\n--with-issues\nowner=\"OWNER\"");
    }
}

/**
 * ${STATIC} Function : save_log
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $msg
 *
 * @return void
 */
function save_log($msg) {
    global $LOG_FILE;
    shell_exec('echo "[INFO]: ' . $msg . PHP_EOL . '" >> ' . $LOG_FILE);
}

/**
 * ${STATIC} Function : warning_log
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $msg
 *
 * @return void
 */
function warning_log($msg) {
    global $LOG_FILE;
    shell_exec('echo "[WARNING]: ' . $msg . PHP_EOL . '" >> ' . $LOG_FILE);
}

/**
 * ${STATIC} Function : my_error_log
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $msg
 *
 * @return void
 */
function my_error_log($msg) {
    global $LOG_FILE;
    shell_exec('echo "[ERROR]: ' . $msg . PHP_EOL . '" >> ' . $LOG_FILE);
}


/**
 * ${STATIC} Function : get_settings
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 *
 * @return void
 */
function get_settings() {
    global $CONFIG;
    $opts_required = [
        'u' => 'USER', 'git-url' => 'git_clone_url',
//        'f' => 'file_to_parse'
    ];
    $opts_optional_bool = [
        'push-all-branches', 'push-all-tags', 'with-wiki',
        'private', 'with-issues', 'create-staging', 'gitosis',
    ];
    $opts_optional_valued = [
        'owner', 'pattern', 'pattern-modifier', 'file', 'simple-repo'
    ];
    $options = getopt('u:h::', [
        'git-url:', 'owner::', 'push-all-branches::', 'push-all-tags::', 'with-wiki::',
        'with-issues::', 'private::', 'help::', 'create-staging::', 'pattern::',
        'pattern-modifier::', 'gitosis::', 'file::', 'simple-repo::',
    ]);
    help($options);
    foreach ($opts_required as $opt => $correspondance) {
        if (!key_exists($opt, $options)) {
            $opt_info = ($opt === 'git-url') ? '--' : '-';
            die($correspondance . " options is required\nPlease use " . $opt_info . $opt . ' ' . $correspondance . '');
        }
    }
    $CONFIG['user'] = $options['u'];
    $CONFIG['password'] = getPassword($options['u']);
    if ($options['git-url'][strlen($options['git-url']) - 1] !== '/') {
        $options['git-url'] .= '/';
    }
    $CONFIG['git_url'] = $options['git-url'];
//    $CONFIG['file'] = $options['f'];
    foreach ($opts_optional_bool as $opt) {
        $CONFIG[$opt] = key_exists($opt, $options);
    }
    foreach ($opts_optional_valued as $opt) {
        if (key_exists($opt, $options)) {
            $CONFIG[$opt] = $options[$opt];
        }
    }

}

/**
 * Function : scanAllDir
 * Created by antoine at 2019-06-22
 * Descr : Scan directory recursively
 * @param $dir
 *
 * @return array
 */
function scanAllDir($dir) {
    $result = [];
    foreach(scandir($dir) as $filename) {
        if ($filename[0] === '.') continue;
        $filePath = $dir . '/' . $filename;
        if (is_dir($filePath)) {
            foreach (scanAllDir($filePath) as $childFilename) {
                $result[] = $filename . '/' . $childFilename;
            }
        } else {
            $result[] = $filename;
        }
    }
    return $result;
}

/**
 * Function : findLanguage
 * Created by antoine at 2019-06-22
 * Descr : Find most used code language in directory
 * @param $path
 *
 * @return bool|string
 */
function findLanguage($path) {
    $exts = [
        'C'           => '\.c',
        'C#'          => '\.cs',
        "C++"         => '\.cpp',
        "Go"          => '\.go',
        "Java"        => '\.java',
        "Objective-C" => '\.h',
        "Perl"        => '\.pl',
        "PHP"         => '\.php',
        "Python"      => '\.py',
        "Ruby"        => '\.rb',
        "Swift"       => '\.swift',
        "JavaScript"  => '\.js',
        "HTML/CSS"    => '\.html',
    ];
    $count = [
        'C'           => 0,
        'C#'          => 0,
        "C++"         => 0,
        "Go"          => 0,
        "Java"        => 0,
        "Objective-C" => 0,
        "Perl"        => 0,
        "PHP"         => 0,
        "Python"      => 0,
        "Ruby"        => 0,
        "Swift"       => 0,
        "JavaScript"  => 0,
        "HTML/CSS"    => 0,
    ];
    $files = scanAllDir($path);
    $otherExts = [];
    foreach ($files as $file) {
        foreach ($exts as $ext => $pattern) {
            if (preg_match('/.*' . $pattern . '$/', $file)) {
                $count[$ext]++;
            } else {
                if (preg_match('/.*(\..*)/', $file, $otherExt)) {
                    $otherExts[] = $otherExt[1];
                }
            }
        }
    }
//    ob_start();
//    var_dump($count);
//    ob_get_clean();
//    save_log('Repository contain: ')
    if (max($count) > 0) {
        $result = array_search(max($count), $count);
        save_log('Count of ' . $result . ' files is ' . max($count));
        return strtolower($result);
    } else {
        warning_log('No match language ! Please see file extensions below:');
        ob_start();
        var_dump($otherExts);
        warning_log(ob_get_clean());
        return false;
    }
}



/**
 * Function : get_BB_repos
 * Created by antoine at 2019-06-21
 * Descr : Check if repository exist in BitBucket
 *
 * @param      $bb_account
 * @param      $bb_password
 *
 * @param bool $owner
 * @param int  $page
 *
 * @return array list of repository [$repository_name => $repository_url]
 */
function get_BB_repos($bb_account, $bb_password, $owner = false, $page = 1) {
    global $colors;
    if ($owner === false) {
        $owner = $bb_account;
    }
    $url = 'https://api.bitbucket.org/2.0/repositories/' . $owner . '/\\?pagelen\\=100\\&page\\=' . $page;
    $bb_repos_request = json_decode(shell_exec('curl --user ' . $bb_account . ':' . $bb_password . ' ' . $url));
//    var_dump('curl --user ' . $bb_account . ':' . $bb_password . ' ' . $url);
//    var_dump($bb_repos_request);
    if (!$bb_repos_request) {
        my_error_log('ERROR Cannot access your repositories');
        my_error_log('Please verify your password/account : ' . $bb_account . '');
        echo $colors->getColoredString('ERROR Cannot access your repositories', 'red') . PHP_EOL;
        echo $colors->getColoredString('Please verify your password/account : ' . $bb_account . '', 'red') . PHP_EOL;
//        echo $colors->getColoredString('Do you want continue without know if these repositories exist ?', 'white') . PHP_EOL;
        if (ask('Do you want continue without know if these repositories exist ?', '/ye?s?/i')) {
            warning_log('Continue without know if these repositories exist');
            return [];
        } else {
            my_error_log('Please verify your password/account : ' . $bb_account . '');
            die($colors->getColoredString('Please verify your password/account : ' . $bb_account . '', 'red') . PHP_EOL);
        }
    }
    $bb_repos = [];
    if (isset($bb_repos_request->values)) {
        $repos_request = $bb_repos_request->values;
    } else {
        $repos_request = $bb_repos_request;
    }

    foreach ($repos_request as $bb_repo) {
        $bb_repos[$bb_repo->name] = $bb_repo->links->html->href;
    }
    if (isset($bb_repos_request->next) && $bb_repos_request->next !== '' ) {
        preg_match('/.*page\=([0-9]+)/', $bb_repos_request->next, $pageNumber);
        if (intval($pageNumber[1]) > $page) {
            $bb_repos = array_merge($bb_repos, get_BB_repos($bb_account, $bb_password, $owner, ++$page));
        } else {
            return $bb_repos;
        }
        return $bb_repos;
    } else {
        return $bb_repos;
    }
}

/**
 * Function : check_repo_exist
 * Created by antoine at 2019-06-21
 * Descr : Check if repository exist in list of repos
 * @param $bb_repos
 * @param $repo
 *
 * @return bool true if exist
 */
function check_repo_exist($bb_repos, $repo) {
    /**
     * Check exist
     */
    if (key_exists($repo, $bb_repos)) {
        warning_log('Repository : ' . $repo . ' exist, you can see it here => ' . $bb_repos[$repo]);
        return true;
    } else {
        save_log('Repository : ' . $repo . ' does not exist it will be created');
        return false;
    }
}

/**
 * ${STATIC} Function : git_checkout
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $base_shell
 * @param $branch
 *
 * @return bool
 */
function git_checkout($base_shell, $branch) {
    global $colors;
    if (shell_exec('git branch') !== '* ' . $branch . PHP_EOL) {
        $error = catchGitError($base_shell . 'git checkout ' . $branch, 'Cannot checkout branch: ' . $branch);
        if ($error) {
            save_log('Change branch to ' . $branch);
            return true;
        } else {
            echo $colors->getColoredString('Cannot checkout branch: ' . $branch, 'red') . PHP_EOL;
        }
    }
}
/**
 * Function : clone_repo
 * Created by antoine at 2019-06-21
 * Descr : Git Clone $repo . '.git' in $base_url
 * @param        $repo
 * @param string $base_url Must end by '/'
 */
function clone_repo($repo, $base_url) {
    global $colors;
    $repo_path = REPO_PATH . $repo;
    if (!is_dir($repo_path)) {
        save_log('Create directory ' . realpath($repo_path));
        shell_exec('mkdir ' . $repo_path);
    } else {
        warning_log('Directory: ' . $repo_path . ' exist !');
    }
    if (!is_dir($repo_path . '/.git')) {
        if (catchGitError('git clone ' . $base_url . $repo . '.git ' . $repo_path, 'Cannot clone repository: ' . $repo)) {
            save_log('Clone ' . $repo . ' in ' . realpath($repo_path));
            echo $colors->getColoredString('Successfully clone repository: ' . $repo, 'green') . PHP_EOL;
        }
    } else {
        echo $colors->getColoredString(realpath($repo_path) . ' already contain .git directory Cannot be cloned', 'red') . PHP_EOL;
        error_log(realpath($repo_path) . ' already contain .git directory Cannot be cloned');
    }
}

/**
 * ${STATIC} Function : push_all_branches
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $base_shell
 * @param $branches
 *
 * @return void
 */
function push_all_branches($base_shell, $branches) {
    global $colors;
    foreach ($branches as $key => $branch) {
        echo $colors->getColoredString('Beginning to push branch : ' . $branch) . PHP_EOL;
        git_checkout($base_shell, $branch);
        save_log('Push BRANCH ' . $branch . ' in BitBucket');
        $start = microtime(true);
        if (catchGitError($base_shell . 'git push -u bb ' . $branch, 'Cannot push Branch: ' . $branch)) {
            $elapsedTime = microtime(true) - $start;
            echo $colors->getColoredString('Branch: ' . $branch . ' pushed in ' . $elapsedTime . 's ' . ($key + 1) . '/' . count($branches), 'green') . PHP_EOL;
            save_log('Branch : ' . $branch . ' pushed in ' . $elapsedTime . 's ' . ($key + 1) . '/' . count($branches));
        } else {
            echo $colors->getColoredString('Cannot pushed branch: ' . $branch . ' ' . ($key + 1) . '/' . count($branches) , 'red') . PHP_EOL;
        }
    }
}

/**
 * ${STATIC} Function : push_all_tags
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $base_shell
 * @param $tags
 *
 * @return void
 */
function push_all_tags($base_shell, $tags) {
    global $colors;
    foreach ($tags as $key => $tag) {
        if ($tag === '') {
            continue;
        }
        $branchNumber = $key + 1;
        echo $colors->getColoredString('Beginning to push tag : ' . $tag) . PHP_EOL;
        save_log('Push TAG ' . $tag . ' in BitBucket');
        $start = microtime(true);
        if (catchGitError($base_shell . 'git push -u bb ' . $tag, 'Cannot push tag: ' . $tag . '')) {
            $elapsedTime = microtime(true) - $start;
            echo $colors->getColoredString('Tag: ' . $tag . ' pushed in ' . $elapsedTime . 's ' . ($key + 1) . '/' . count($tags), 'green') . PHP_EOL;
            save_log('Tag : ' . $tag . ' pushed in ' . $elapsedTime . 's ' . $branchNumber . '/' . count($tags));
        } else {
            echo $colors->getColoredString('Cannot push tag: ' . $tag . ' ' . $branchNumber . '/' . count($tags), 'red') . PHP_EOL;
        }
    }
}

/**
 * ${STATIC} Function : add_remote
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param      $repo
 * @param      $bb_account
 * @param bool $owner
 * @param bool $push_all_branches
 * @param bool $push_all_tags
 * @param bool $create_staging
 *
 * @return void
 */
function add_remote($repo, $bb_account, $owner = false, $push_all_branches = false, $push_all_tags = false, $create_staging = false) {
    global $current_step, $colors;
    $dir = REPO_PATH . $repo;
    $owner = ($owner !== false) ? $owner : $bb_account;
    $base_shell = 'cd ' . $dir . ' && ';
    if (!in_array('bb', explode(PHP_EOL, shell_exec($base_shell . 'git remote')))) {
        echo $colors->getColoredString('Begin add remote bb') . "\n";
        save_log('Add remote bb in ' . $repo . ' to ' . $owner . ' BitBucket account');
        save_log('remote : ' . 'https://' . $bb_account . '@bitbucket.org/' . $owner . '/' . $repo . '.git');
        $start = microtime(true);
        if (catchGitError($base_shell . 'git remote add bb https://' . $bb_account . '@bitbucket.org/' . $owner . '/' . $repo . '.git',
            'Cannot add remote at ' . $owner . ' in ' . $repo . ' repository with account : ' . $bb_account)) {
            $elapsedTime = microtime(true) - $start;
            echo $colors->getColoredString('Remote bb added in ' . $elapsedTime . 's', 'green') . PHP_EOL;
        } else {
            echo $colors->getColoredString('Cannot add remote: bb in ' . $repo, 'red') . PHP_EOL;
        }
    } else {
        echo $colors->getColoredString('Remote bb already exist will use it !', 'yellow') . PHP_EOL;
        warning_log('Remote bb already exist');
    }
    $branches_str = shell_exec($base_shell . 'git branch -a');
    preg_match_all('/remotes\/[^\/]*\/([^\s]*)\n/m', $branches_str, $branches);
    echo $colors->getColoredString('Branches founds : ' . implode(', ', $branches[1]), 'green') . PHP_EOL;
    save_log('Branches founds : ');
    ob_start();
    var_dump($branches[1]);
    save_log(ob_get_clean());
    if ($push_all_branches) {
//        $current_step = STEP['push_branches'];
        push_all_branches($base_shell, $branches[1]);
    }
    $tags = explode(PHP_EOL, shell_exec($base_shell . 'git tag'));
    save_log('Tags founds : ');
    unset($tags[array_search('', $tags)]);
    if (!empty($tags)) {
        echo $colors->getColoredString('Tags founds : ' . implode(', ', $tags), 'green') . PHP_EOL;
        ob_start();
        var_dump($tags);
        save_log(ob_get_clean());
    } else {
        echo $colors->getColoredString('No tags founds !', 'warning') . PHP_EOL;
        ob_start();
        var_dump($tags);
        warning_log('No tags founds !');
    }
    if ($push_all_tags) {
//        $current_step = STEP['push_tags'];
        push_all_tags($base_shell, $tags);
    }
    if ($create_staging) {
        if (!in_array('staging', $branches[1])) {
//            $current_step = STEP['create_staging'];
            git_checkout($base_shell, 'master');
            save_log('Create staging Branch ');

            echo $colors->getColoredString('Beginning to create staging branch') . PHP_EOL;

            $start = microtime(true);
            if (catchGitError($base_shell . 'git checkout -b staging', 'Cannot create staging branch in ' . $repo . ' repository')) {

                $elapsedTime = microtime(true) - $start;
                echo $colors->getColoredString('staging branch created in ' . $elapsedTime . 's', 'green') . PHP_EOL;
            } else {
                echo $colors->getColoredString('Cannot create staging branch in repository: ' . $repo, 'red') . PHP_EOL;
            }
        } else {
            git_checkout($base_shell, 'staging');
        }
//        $current_step = STEP['push_staging'];
        echo $colors->getColoredString('Beginning to push branch : staging ');

        $start = microtime(true);
        if (catchGitError($base_shell . 'git push -u bb staging', 'Cannot push staging branch in repository: ' . $repo)) {
            $elapsedTime = microtime(true) - $start;
            echo $colors->getColoredString('Branch : staging pushed in ' . $elapsedTime . 's', 'green') . PHP_EOL;
            save_log('Pushed staging Branch in ' . $elapsedTime . 's');
        } else {
            echo $colors->getColoredString('Cannot push staging branch in repository: ' . $repo, 'red') . PHP_EOL;
        }

    }
}


/**
 * Function : create_bb_repo
 * Created by antoine at 2019-06-21
 * Descr :
 *
 * @param array $opts [
 *                    'is_private' => true,
 *                    'name' => 'REPO_NAME',
 *                    'has_wiki' => true,
 *                    'has_issues' => true,
 *                    'language' => 'php',
 *                    ]
 */
function create_bb_repo($name, array $opts) {
    global $CONFIG;
    $owner = $CONFIG['user'];
    if (key_exists('owner', $opts)) {
        $owner = $opts['owner'];
    }

    save_log('Create repository ' . $name . ' with these options : ');
    ob_start();
    var_dump($opts);
    save_log(ob_get_clean());
    global $colors;

    $repoName = strtolower(preg_replace('/[^\.\_\-[A-Za-z][0-9]]/', '', $name));
    echo $colors->getColoredString('Beginning to create repository ' . $repoName);
    $start = microtime(true);
    $command = 'curl -X POST -u ' . $CONFIG['user'] . ':' . $CONFIG['password'] . ' "https://api.bitbucket.org/2.0/repositories/' . $owner . '/' . $repoName . '" -H "Content-Type: application/json" -d \'' . json_encode($opts) . '\'';
    $curlCreate = shell_exec($command);
    $elapsedTime = microtime(true) - $start;

    $result = json_decode($curlCreate);
    if ($result->type === 'error') {
        echo $colors->getColoredString('ERROR Cannot create repository : ' . $repoName, 'red') . PHP_EOL;
        my_error_log('Cannot create repository : ' . $repoName);
        return false;
    } else {
        echo $colors->getColoredString('Repository : ' . $repoName . ' created in ' . $elapsedTime . 's', 'green') . PHP_EOL;
        save_log('Repository : ' . $repoName . ' created in ' . $elapsedTime . 's');
        return true;
    }
//    var_dump($command);



}

/**
 * ${STATIC} Function : catchGitError
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $command
 * @param $errorMsg
 *
 * @return bool
 */
function catchGitError($command, $errorMsg) {
//    die(var_dump(shell_exec($command . ' >/dev/null; echo $?')));
    if (shell_exec($command . ' >/dev/null; echo $?') == 0) {
        return true;
    } else {
        my_error_log($errorMsg);
        return false;
    }
}

function getPassword($user, $prompt = "Enter Password:") {
    global $colors;
    echo $colors->getColoredString($prompt, 'white');

    system('stty -echo');

    $password = trim(fgets(STDIN));

    system('stty echo');

    echo $colors->getColoredString(PHP_EOL . 'Verify connexion to your ' . $user . '\'s bitbucket account ...', 'white') . PHP_EOL;
//    var_dump($password);
    $command = 'curl --user ' . $user . ':' . $password . ' "https://api.bitbucket.org/2.0/repositories/' . $user . '/tagada"';
    $shell = json_decode(shell_exec($command));
//    var_dump($shell);
    if ($shell) {
        return $password;
    } else {
        echo $colors->getColoredString('ERROR Password incorrect for ' . $user . '\'s bitbucket account !', 'red') . PHP_EOL;
        return getPassword($user, $prompt);
    }
}
function ask($prompt, $pattern = '/.*/') {
    global $colors;

    $result = readline($colors->getColoredString($prompt, 'white'));

    return preg_match($pattern, $result);

}
/**
 * ${STATIC} Function : getRepositories
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 *
 * @return array|bool|mixed
 */
function getRepositories() {
    global $CONFIG, $colors;

    if (isset($CONFIG['simple-repo'])) {
        return [$CONFIG['simple-repo']];
    } else if (isset($CONFIG['file']) && $CONFIG['gitosis']) {
        $gitosis_content = file_get_contents($CONFIG['file']);
        preg_match_all('/\[group\s([^\s]*)\]\s*writable/m', $gitosis_content, $repos);
        if (isset($repos[1]) && is_array($repos) &&
            is_array($repos[1]) && !empty($repos[1])
        ) {
            return $repos[1];
        } else {
            echo $colors->getColoredString('Gitosis pattern does not match ! ' . PHP_EOL . 'Please verify your gitosis file : ' . $CONFIG['file'] . PHP_EOL, 'red');
            return false;
        }
    } else if (isset($CONFIG['pattern']) && isset($CONFIG['file'])) {
        $file = file_get_contents($CONFIG['file']);
        $pattern = $CONFIG['pattern'];
        $modifier = '';
        if (isset($CONFIG['pattern-modifier'])) {
            $modifier = $CONFIG['pattern-modifier'];
        }
        if ($pattern[0] !== '/' && $pattern[strlen($pattern) - 1] !== '/') {
            $pattern = '/' . $pattern . '/';
        }
        $pattern .= $modifier;
        preg_match_all($pattern, $file, $repos);
        if (isset($repos[1]) && is_array($repos) &&
            is_array($repos[1]) && !empty($repos[1])
        ) {
            return $repos[1];
        } else {
            echo $colors->getColoredString('Your pattern : ' . $pattern . ' do not match in your file : ' . $CONFIG['file'] . PHP_EOL, 'red');
            return false;
        }

    } else if (isset($CONFIG['file'])) {
        if (preg_match('/\.json$/', $CONFIG['file']) ) {
            $file = file_get_contents($CONFIG['file']);
            $json = json_decode($file);
            if (is_array($json) && !empty($json)) {
                return $json;
            } else {
                echo $colors->getColoredString('Your json file must be an array encoded in json format like sample.json' . PHP_EOL, 'red');
                return false;
            }
        } else {
            echo $colors->getColoredString('file must be a json file if you use only file option' . PHP_EOL, 'red');
            return false;
        }
    } else {
        echo $colors->getColoredString('You must use one of available options like : file pattern ...' . PHP_EOL, 'red');
        return false;
    }
}
$current_repo = '';
/**
 * ${STATIC} Function : start
 * Created by antoine at 2019-07-23
 * Descr : @todo type description
 *
 * @param $conf_file
 *
 * @return void
 */
function main() {
    global $CONFIG, $current_repo, $current_step, $colors, $current_repo_url;
    get_settings();
    $repos = getRepositories();
    if ($repos === false) {
        die($colors->getColoredString('Please see help with -h options', 'red'));
    }
    if (!key_exists('owner', $CONFIG)) {
        $CONFIG['owner'] = $CONFIG['user'];
    }
    $bb_repos = get_BB_repos($CONFIG['user'], $CONFIG['password'], $CONFIG['owner']);
//    die(var_dump($bb_repos));
    foreach ($repos as $key => $repo) {
        if (check_repo_exist($bb_repos, $repo)) {
            echo $colors->getColoredString('Skip repository : ' . $repo . ' (exist in BitBucket)' . PHP_EOL . 'Please see it here -> ' . $bb_repos[$repo] . PHP_EOL, 'yellow');
            continue;
        }
        echo $colors->getColoredString('Beginning repository : ' . $repo . ' ' . $key . '/' . count($repos), 'green') . PHP_EOL;
        $current_repo = $repo;
        $current_step = STEP['clone'];
        $start = microtime(true);
        clone_repo($repo, $CONFIG['git_url']);
        $elapsedTime = microtime(true) - $start;
        echo $colors->getColoredString('Repository . ' . $repo . ' cloned with success in ' . $elapsedTime . 's', 'green') . PHP_EOL;
        save_log('Repository . ' . $repo . ' cloned with success in ' . $elapsedTime . 's' . '(' . REPO_PATH . $repo . ')');
        $language = findLanguage(REPO_PATH . $repo);
        $bb_repo_options = [
            'is_private' => $CONFIG['private'],
            'has_wiki'   => $CONFIG['with-wiki'],
            'has_issues' => $CONFIG['with-issues'],
        ];
        if ($language !== false) {
            $bb_repo_options['language'] = $language;
        }
        if (key_exists('owner', $CONFIG)) {
            $bb_repo_options['owner'] = $CONFIG['owner'];
        }
        $current_step = STEP['create_repo'];
        if (!create_bb_repo($repo, $bb_repo_options)) {
            continue;
        }
        $current_step = STEP['add_remote'];
        if (key_exists('owner', $CONFIG)) {
            $current_repo_url = 'https://bitbucket.org/' . $CONFIG['owner'] . '/' . $repo;
            add_remote($repo, $CONFIG['user'], $CONFIG['owner'], $CONFIG['push-all-branches'], $CONFIG['push-all-tags'], $CONFIG['create-staging']);
        } else {
            $current_repo_url = 'https://bitbucket.org/' . $CONFIG['user'] . '/' . $repo;
            add_remote($repo, $CONFIG['user'], false, $CONFIG['push-all-branches'], $CONFIG['push-all-tags'], $CONFIG['create-staging']);
        }
    }
}


main();

